CREATE DATABASE IF NOT EXISTS aplicacion3;
USE aplicacion3;


CREATE TABLE coches (
  ID int(10) NOT NULL AUTO_INCREMENT,
  marca varchar(50) DEFAULT NULL,
  fecha date DEFAULT NULL,
  precio float DEFAULT NULL,
  PRIMARY KEY (ID)
)
ENGINE = INNODB,
CHARACTER SET utf8,
COLLATE utf8_spanish_ci;

CREATE TABLE clientes (
  cod int(10) NOT NULL AUTO_INCREMENT,
  nombre varchar(50) DEFAULT NULL,
  idcochealquilado int(10) NOT NULL,
  Fechaalquiler date DEFAULT NULL,
  PRIMARY KEY (cod)
)
ENGINE = INNODB,
CHARACTER SET utf8,
COLLATE utf8_spanish_ci;

ALTER TABLE clientes
ADD UNIQUE INDEX UK_clientes_idcochealquilado (idcochealquilado);

ALTER TABLE clientes
ADD CONSTRAINT FK_clientes_idcochealquilado FOREIGN KEY (idcochealquilado)
REFERENCES aplicacion3.coches (ID) ON DELETE CASCADE ON UPDATE CASCADE;